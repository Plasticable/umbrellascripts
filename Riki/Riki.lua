---
--- Riki combo
---
local Riki = {}
Riki.isComboEnabled = Menu.AddOptionBool({"Hero Specific", "Riki"}, "Dart + Meteor + Smoke Screen Combo", false)
Riki.isDebugEnabled = Menu.AddOptionBool({"Hero Specific", "Riki"}, "Debug messages", false)
Riki.comboKey = Menu.AddKeyOption({"Hero Specific", "Riki"}, "Key", Enum.ButtonCode.KEY_NONE)
Menu.AddMenuIcon({"Hero Specific", "Riki"}, "panorama/images/heroes/icons/npc_dota_hero_Riki_png.vtex_c");

local myHero
local dartAbility
local smokeScreenAbility

local gameTime = 0

local combo = {}
combo.inProgress = false
combo.meteorTimer = 0
combo.smokeScreenTimer = 0
combo.position = nil

local timings = {}
timings.dartStunDuration = 4
timings.meteorCastTime = 2.5
timings.meteorStunDuration = 1.5

function Riki.PrintDebug(msg)
    if Menu.IsEnabled(Riki.isDebugEnabled) then
        Log.Write(msg)
    end
end

function Riki.StartCombo(comboTarget)
    if not dartAbility then
        Riki.PrintDebug("Dart ability is nil!")
        return
    end

    if not Riki.CanInteract(myHero, comboTarget, "SkillOnTarget", dartAbility, NPC.GetMana(myHero), true) then
        return
    end

    Riki.PrintDebug("Casting Dart")
    Ability.CastTarget(dartAbility, comboTarget, false, false)

    combo.inProgress = true
    combo.position = Entity.GetOrigin(comboTarget)

    Riki.PrintDebug("Scheduling Meteor Hammer cast")
    combo.meteorTimer = gameTime + timings.dartStunDuration - timings.meteorCastTime - 0.1
end

function Riki.OnUpdate()
    if GameRules.IsPaused() or not Menu.IsEnabled(Riki.isComboEnabled) or not myHero then
        return
    end

    gameTime = GameRules.GetGameTime()

    if combo.inProgress then
        if combo.meteorTimer ~= 0 and combo.meteorTimer <= gameTime then
            local meteorItem = NPC.GetItem(myHero, "item_meteor_hammer")

            -- if NPC.HasItem(myHero, "item_meteor_hammer") then
            if meteorItem and Riki.CanInteract(myHero, nil, "ItemNoTarget", meteorItem, NPC.GetMana(myHero), true) then
                Riki.PrintDebug("Casting meteor")
                Ability.CastPosition(meteorItem, combo.position)
            end

            combo.meteorTimer = 0

            Riki.PrintDebug("Scheduling Smoke Screen cast")
            combo.smokeScreenTimer = gameTime + timings.meteorCastTime + timings.meteorStunDuration - 0.1
        end

        if combo.smokeScreenTimer ~= 0 and combo.smokeScreenTimer <= gameTime then
            if Riki.CanInteract(myHero, nil, "SkillNoTarget", smokeScreenAbility, NPC.GetMana(myHero), true) then
                Riki.PrintDebug("Casting smoke screen")
                Ability.CastPosition(smokeScreenAbility, combo.position)
            end

            combo.smokeScreenTimer = 0
            combo.inProgress = false
            combo.position = nil
        end

        return
    end

    if not Menu.IsKeyDownOnce(Riki.comboKey) then
        return
    end

    local comboTarget = Input.GetNearestHeroToCursor(Entity.GetTeamNum(myHero), Enum.TeamType.TEAM_ENEMY)

    Riki.PrintDebug("Possible target: " .. Entity.GetClassName(comboTarget))
    
    if comboTarget and not NPC.IsIllusion(comboTarget) then
        Riki.PrintDebug("Starting combo at " .. gameTime)
        Riki.StartCombo(comboTarget)
        return
    end
end

function Riki.Init()
    if not Engine.IsInGame then
        return
    end

    myHero = Heroes.GetLocal()

    if NPC.GetUnitName(myHero) ~= "npc_dota_hero_riki" then
        myHero = nil
        return
    end

    dartAbility = NPC.GetAbility(myHero, "riki_poison_dart")
    smokeScreenAbility = NPC.GetAbility(myHero, "riki_smoke_screen")
end

function Riki.UnInit()
    myHero = nil
    dartAbility = nil
    smokeScreenAbility = nil
end

function Riki.OnGameStart()
    Riki.Init();
end

function Riki.OnEntityDestroy(entity)
    if entity and myHero == entity then
        Riki.Init()
    end
end

function Riki.OnGameEnd()
    Riki.UnInit();
end

Riki.Init()

-- by Dr_1337
function Riki.CanInteract(Source, Target, Type, Abil, Mana, Magic)
    if Source and Entity.IsAlive(Source) and (not Target or (Entity.IsAlive(Target) and not Entity.IsDormant(Target))) and
        (not Abil or
            (Abilities.Contains(Abil) and not Ability.IsPassive(Abil) and Mana and Ability.IsCastable(Abil, Mana))) and
        not NPC.IsStunned(Source) and not NPC.HasState(Source, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) and
        (not Magic or not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_MAGIC_IMMUNE)) then
        if Type == "UtilityMove" then -- or (can move)
            if not NPC.HasState(Source, Enum.ModifierState.MODIFIER_STATE_ROOTED) then
                return true
            end
        elseif Type == "UtilityAttack" then -- or (can attack)
            if not NPC.HasState(Source, Enum.ModifierState.MODIFIER_STATE_DISARMED) then
                return true
            end
        elseif Type == "UtilitySkill" then -- or (can use skill)
            if Abil and not NPC.IsSilenced(Source) then
                return true
            end
        elseif Type == "UtilityItem" then -- or (can use item)
            if Abil then
                return true
            end
        elseif Type == "Move" then -- or (can move to target)
            if not NPC.HasState(Source, Enum.ModifierState.MODIFIER_STATE_ROOTED) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) then
                return true
            end
        elseif Type == "Attack" then -- or (can attack target)
            if not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_INVULNERABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_UNTARGETABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_ATTACK_IMMUNE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) and
                not NPC.HasState(Source, Enum.ModifierState.MODIFIER_STATE_DISARMED) then
                return true
            end
        elseif Type == "SkillNoTarget" then -- or (can use 'areaTarget' skill on target(target position))
            if Abil and not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_INVULNERABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) and not NPC.IsSilenced(Source) then
                return true
            end
        elseif Type == "SkillOnTarget" then -- or (can use 'targetUnit' skill on target)
            if Abil and not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_INVULNERABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_UNTARGETABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) and not NPC.IsSilenced(Source) then
                return true
            end
        elseif Type == "ItemNoTarget" then -- or (can use 'areaTarget' item on target(target position))
            if Abil and not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_INVULNERABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) then
                return true
            end
        elseif Type == "ItemOnTarget" then -- or (can use 'targetUnit' item on target)
            if Abil and
                (not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_INVULNERABLE) or Ability.GetName(Abil) ==
                    "item_nullifier") and not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_UNTARGETABLE) and
                not NPC.HasState(Target, Enum.ModifierState.MODIFIER_STATE_OUT_OF_GAME) then
                return true
            end
        end
    end
    return false
end

return Riki
